package com.example.weathertest.ui.cities

import android.os.Bundle
import android.view.View
import android.widget.SearchView
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.weathertest.App
import com.example.weathertest.BaseFragment
import com.example.weathertest.R
import com.example.weathertest.data.network.objects.City
import com.example.weathertest.ui.cities.adapters.CitiesAdapter
import com.example.weathertest.utils.EventLiveData
import com.example.weathertest.utils.toast
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_cities.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import javax.inject.Inject


class CitiesFragment : BaseFragment(), CitiesView {

    @Inject
    @InjectPresenter
    lateinit var presenter: CitiesPresenter

    @ProvidePresenter
    fun providePresenter() = presenter

    lateinit var mItemTouchHelper: ItemTouchHelper

    companion object {
        fun newInstance() =
            CitiesFragment().apply {
                arguments = Bundle().apply {}
            }
    }

    override val layoutId = R.layout.fragment_cities


    override fun onCreate(savedInstanceState: Bundle?) {
        App.mainComponent().inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        cities_refresh.setOnRefreshListener {
            presenter.updateCities()
        }

        cities_search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                cities_search.setQuery("", false)
                cities_search.clearFocus()
                cities_search.onActionViewCollapsed()
                val cleanValue = query.replace(" ", "").toLowerCase().capitalize()
                presenter.addCity(cleanValue)
                return false
            }
        })

    }


    override fun showFoundedCities(cities: List<City>) {
    }

    override fun showMessage(message: String) {
        if (message.contains("already")) {
            Snackbar.make(cities_root, message, Snackbar.LENGTH_LONG)
                .show()
        } else {
            Snackbar.make(cities_root, "Internet connection error.", Snackbar.LENGTH_LONG)
                .show()
        }

    }

    override fun showProgress(isEnabled: Boolean) {
        cities_refresh.isRefreshing = false
        EventLiveData.progress.postValue(isEnabled)
    }

    override fun showCities(cities: List<City>) {
        cities_list.layoutManager = LinearLayoutManager(context)
        val adapter = CitiesAdapter(cities as MutableList<City>) { id, name ->
            findNavController().navigate(
                CitiesFragmentDirections.toCityDetails(
                    cityId = id,
                    name = name
                )
            )
        }
        cities_list.adapter = adapter
    }


}