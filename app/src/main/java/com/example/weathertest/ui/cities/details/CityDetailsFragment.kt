package com.example.weathertest.ui.cities.details

import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.weathertest.App
import com.example.weathertest.BaseFragment
import com.example.weathertest.R
import com.example.weathertest.data.network.objects.City
import com.example.weathertest.data.network.objects.CurrentWeather
import com.example.weathertest.data.network.objects.Forecast
import com.example.weathertest.ui.cities.adapters.ForecastAdapter
import com.example.weathertest.utils.Constants
import com.example.weathertest.utils.EventLiveData
import com.example.weathertest.utils.toast
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_cities.*
import kotlinx.android.synthetic.main.fragment_city_details.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import java.text.SimpleDateFormat
import java.time.*
import java.time.format.DateTimeFormatter
import java.util.*
import javax.inject.Inject
import kotlin.math.roundToInt


class CityDetailsFragment : BaseFragment(), CityDetailsView {

    @Inject
    @InjectPresenter
    lateinit var presenter: CityDetailsPresenter

    @ProvidePresenter
    fun providePresenter() = presenter

    private val args: CityDetailsFragmentArgs by navArgs()
    override val layoutId = R.layout.fragment_city_details


    override fun onCreate(savedInstanceState: Bundle?) {
        App.mainComponent().inject(this)
        super.onCreate(savedInstanceState)
        presenter.init(args.cityId)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        swipe_refresh.setOnRefreshListener {
            presenter.init(args.cityId)

        }

        cities_details_forecast_list.layoutManager = LinearLayoutManager(context)
        city_details_delete.setOnClickListener {
            presenter.deleteAndClose(args.cityId)
        }
    }

    override fun showMessage(message: String) {

    }

    override fun showProgress(isEnabled: Boolean) {
        swipe_refresh.isRefreshing = false
        EventLiveData.progress.postValue(isEnabled)
    }

    override fun showDetails(forecast: Forecast) {

        Picasso.get()
            .load(Constants.getAssetPath(forecast.current.weather.first().main.toLowerCase(Locale.getDefault())))
            .into(imageView)
        Picasso.get()
            .load(Constants.getWeatherIcon(forecast.current.weather.first().icon))
            .into(city_details_icon)
        city_details_title.text = args.name
        city_details_degree.text =
            getString(
                R.string.degree_value,
                forecast.current.temp.roundToInt().toString()
            )
        city_details_weather_state.text = forecast.current.weather.first().description
        city_details_wind.text =
            getString(R.string.wind_value, forecast.current.windSpeed.toString())
        city_details_pressure.text =
            getString(R.string.pressure_value, forecast.current.pressure.toString())
        city_detail_humidity.text =
            getString(R.string.humidity_value, forecast.current.humidity.toString())

        imageView.setColorFilter(Color.GRAY, PorterDuff.Mode.DARKEN)
        cities_details_forecast_list.adapter = ForecastAdapter(forecast.daily)

    }

    override fun goBack() {
        findNavController().popBackStack()
    }

    override fun showLocalDetails(city: City) {
        city_details_title.text = city.name
        city_details_degree.text =
            getString(
                R.string.degree_value,
                city.weatherMain?.temp?.roundToInt().toString()
            )
        city_details_weather_state.text = city.weather?.first()?.main ?: ""
        city_details_wind.text =
            getString(R.string.wind_value, city.wind?.speed.toString())
        city_details_pressure.text =
            getString(R.string.pressure_value, city.weatherMain?.pressure.toString())
        city_detail_humidity.text =
            getString(R.string.humidity_value, city.weatherMain?.humidity.toString())

        Picasso.get()
            .load("file:///android_asset/${city.weather?.first()?.main?.toLowerCase(Locale.getDefault())}.jpeg")
            .into(imageView)
        imageView.setColorFilter(Color.GRAY, PorterDuff.Mode.DARKEN)
    }


}