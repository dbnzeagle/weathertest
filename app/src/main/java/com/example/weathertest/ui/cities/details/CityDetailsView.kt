package com.example.weathertest.ui.cities.details

import com.example.weathertest.data.network.objects.City
import com.example.weathertest.data.network.objects.CurrentWeather
import com.example.weathertest.data.network.objects.Forecast
import com.example.weathertest.utils.ImageEnum
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface CityDetailsView : MvpView {
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)
    fun showProgress(isEnabled: Boolean)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showDetails(forecast: Forecast)

    fun goBack()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showLocalDetails(city: City)
}