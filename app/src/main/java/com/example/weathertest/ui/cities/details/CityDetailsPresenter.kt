package com.example.weathertest.ui.cities.details

import com.example.weathertest.BasePresenter
import com.example.weathertest.data.repository.Repository
import com.example.weathertest.data.repository.city.CityRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class CityDetailsPresenter @Inject constructor(
    private val repository: Repository,
    private val cityRepository: CityRepository
) : BasePresenter<CityDetailsView>() {

    fun init(cityId: Long) {
        addDisposable(
            cityRepository.getCity(cityId)
                .flatMap {
                    repository.getForecast(it.coordinates!!)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress(true) }
                .doAfterTerminate { viewState.showProgress(false) }
                .subscribe({ currentWeather ->
                    viewState.showDetails(currentWeather)
                }, { e ->
                    getLocalData(cityId)
                })
        )
    }

    fun getLocalData(id:Long){
        addDisposable(
            cityRepository.getCity(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress(true) }
                .doAfterTerminate { viewState.showProgress(false) }
                .subscribe({ cityInfo ->
                    viewState.showLocalDetails(cityInfo)
                    viewState.showMessage("")
                }, { e ->
                    Timber.e(e)
                })
        )
    }

    fun deleteAndClose(cityId: Long) {
        addDisposable(
            cityRepository.getCity(cityId)
                .flatMapCompletable {
                    cityRepository.deleteCity(it)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    viewState.goBack()
                }, { e -> Timber.e(e) })

        )
    }

}