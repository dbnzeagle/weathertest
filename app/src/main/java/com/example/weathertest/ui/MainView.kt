package com.example.weathertest.ui

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface MainView :MvpView{

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)
    fun showProgress(isEnabled: Boolean)
}