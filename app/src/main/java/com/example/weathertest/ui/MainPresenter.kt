package com.example.weathertest.ui

import com.example.weathertest.BasePresenter
import com.example.weathertest.data.network.objects.City
import com.example.weathertest.data.repository.Repository
import com.example.weathertest.data.repository.city.CityRepository
import com.example.weathertest.di.MainScope
import com.example.weathertest.utils.Constants
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import moxy.InjectViewState
import java.util.*
import javax.inject.Inject

@MainScope
@InjectViewState
class MainPresenter @Inject constructor(
    private val cityRepository: CityRepository,
    private val repository: Repository
) : BasePresenter<MainView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        addDisposable(
            cityRepository.getCities()
                .flatMap { repository.getCities(it) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress(true) }
                .doAfterTerminate { viewState.showProgress(false) }
                .subscribe({},{e->
                    viewState.showMessage(e.message!!)
                })

        )
    }
}