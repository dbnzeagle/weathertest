package com.example.weathertest.ui.cities.adapters

import android.graphics.Color
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.weathertest.R
import com.example.weathertest.data.network.objects.CurrentWeather
import com.example.weathertest.data.network.objects.ForecastDetail
import com.example.weathertest.utils.Constants
import com.squareup.picasso.Picasso
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

class ForecastAdapter(
    private val items: List<ForecastDetail>
) : RecyclerView.Adapter<ForecastAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.forecast_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.day.text = SimpleDateFormat("EEEE", Locale.ENGLISH).format(Date(item.dt*1000))
        holder.degree.text = holder.itemView.context.getString(
            R.string.degree_value,
            item.temp.day.roundToInt().toString()
        )
        Picasso.get()
            .load(Constants.getWeatherIcon(item.weather.first().icon))
            .into(holder.image)
    }

    override fun getItemCount(): Int = items.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val day: TextView = view.findViewById(R.id.forecast_item_state)
        val degree: TextView = view.findViewById(R.id.forecast_item_temp)
        val image: ImageView = view.findViewById(R.id.forecast_item_icon)
    }
}