package com.example.weathertest.ui.cities

import com.example.weathertest.data.network.objects.City
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface CitiesView : MvpView {
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)
    fun showProgress(isEnabled: Boolean)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showCities(cities: List<City>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showFoundedCities(cities: List<City>)
}