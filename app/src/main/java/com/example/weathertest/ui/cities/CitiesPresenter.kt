package com.example.weathertest.ui.cities

import com.example.weathertest.BasePresenter
import com.example.weathertest.data.repository.city.CityRepository
import com.example.weathertest.data.repository.Repository
import com.example.weathertest.data.repository.city.CityRepositoryImpl
import com.example.weathertest.utils.Constants
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import moxy.InjectViewState
import timber.log.Timber
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


@InjectViewState
class CitiesPresenter @Inject constructor(
    private val cityRepository: CityRepository,
    private val repository: Repository
) : BasePresenter<CitiesView>() {

    override fun attachView(view: CitiesView?) {
        super.attachView(view)
        loadCities()
    }


    private fun loadCities() {
        addDisposable(
            cityRepository.getCities()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress(true) }
                .doAfterTerminate { viewState.showProgress(false) }
                .subscribe({ response ->
                    viewState.showCities(response)
                }, { e ->
                    Timber.e(e)
                    viewState.showMessage(e.message!!)
                })

        )
    }

    fun updateCities() {
        addDisposable(
            cityRepository.getCities()
                .flatMap {
                    repository.getCities(it)
                }
                .flatMap {
                    cityRepository.addCities(it.list).toSingle { it.list }
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress(true) }
                .doAfterTerminate { viewState.showProgress(false) }
                .subscribe({
                    viewState.showCities(it)
                }, { e ->
                    Timber.e(e)
                    viewState.showMessage(e.message!!)
                })
        )
    }

    fun addCity(q: String) {
        addDisposable(
            cityRepository.getCities()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress(true) }
                .doAfterTerminate { viewState.showProgress(false) }
                .subscribe({
                    if (it.find { city -> city.name == q } != null) {
                        viewState.showMessage("City already added")
                        viewState.showCities(it)
                    } else {
                        fetchNewCity(q)
                    }
                }, { e ->
                    Timber.e(e)
                    viewState.showMessage(e.message.toString())
                })
        )

    }

    fun fetchNewCity(q: String) {
        addDisposable(
            repository.searchCity(q)
                .flatMap { cityResponse ->
                    cityRepository.addCity(cityResponse.list.first {
                        it.name.toLowerCase(Locale.getDefault()) == q.toLowerCase(
                            Locale.getDefault()
                        )
                    })
                        .andThen(cityRepository.getCities())
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress(true) }
                .doAfterTerminate { viewState.showProgress(false) }
                .subscribe({
                    viewState.showCities(it)
                }, { e ->
                    Timber.e(e)
                    viewState.showMessage(e.message.toString())
                })
        )
    }
}