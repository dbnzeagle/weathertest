package com.example.weathertest.ui

import android.os.Bundle
import androidx.lifecycle.Observer
import com.example.weathertest.App
import com.example.weathertest.BaseActivity
import com.example.weathertest.R
import com.example.weathertest.utils.EventLiveData
import com.example.weathertest.utils.LoaderDialog
import com.example.weathertest.utils.toast
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import javax.inject.Inject

class MainActivity : BaseActivity(), MainView {

    @Inject
    @InjectPresenter
    lateinit var presenter: MainPresenter

    @ProvidePresenter
    fun providePresenter() = presenter


    override val layoutId = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        App.initMainComponent()?.inject(this)
        super.onCreate(savedInstanceState)
        initEventData()
    }

    private fun initEventData(){
        EventLiveData.progress.observe(this, Observer {
            LoaderDialog.setVisibility(this, it)
        })
    }

    override fun showMessage(message: String) {
        Snackbar.make(activity_root, "Internet connection error.", Snackbar.LENGTH_LONG)
            .show()
    }

    override fun showProgress(isEnabled: Boolean) {
        EventLiveData.progress.postValue(isEnabled)
    }
}