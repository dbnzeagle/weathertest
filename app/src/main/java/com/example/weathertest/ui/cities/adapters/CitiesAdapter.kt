package com.example.weathertest.ui.cities.adapters

import android.graphics.Color
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.weathertest.R
import com.example.weathertest.data.network.objects.City
import com.example.weathertest.utils.Constants
import com.example.weathertest.utils.Constants.getAssetPath
import com.squareup.picasso.Picasso


class CitiesAdapter(
    private var cities: MutableList<City>, private val onClick: (Long, String) -> Unit
) : RecyclerView.Adapter<CitiesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.city_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = cities[position]
        holder.cityTitle.text = item.name
        holder.weatherState.text = item.weather?.first()?.main ?: ""
        holder.degree.text = holder.itemView.context.getString(
            R.string.degree_value,
            item.weatherMain?.temp?.toInt().toString()
        )
        holder.container.setOnClickListener { onClick.invoke(item.id, item.name) }
        Picasso.get()
            .load(getAssetPath(item.weather?.first()?.main!!))
            .into(holder.image)
        Picasso.get()
            .load(Constants.getWeatherIcon(item.weather.first().icon))
            .into(holder.icon)
        holder.image.setColorFilter(Color.GRAY, PorterDuff.Mode.DARKEN)
    }

    override fun getItemCount(): Int = cities.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val cityTitle: TextView = view.findViewById(R.id.city_item_title)
        val weatherState: TextView = view.findViewById(R.id.city_item_subtitle)
        val degree: TextView = view.findViewById(R.id.city_item_degree)
        val image: ImageView = view.findViewById(R.id.city_item_image)
        val icon: ImageView = view.findViewById(R.id.city_item_icon)
        val container: CardView = view.findViewById(R.id.city_item_card_view)
    }
}