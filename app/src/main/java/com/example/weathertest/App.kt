package com.example.weathertest

import android.app.Application
import com.example.weathertest.di.AppComponent
import com.example.weathertest.di.DaggerAppComponent
import com.example.weathertest.di.MainComponent
import com.example.weathertest.di.modules.ContextModule

class App : Application() {
    companion object {
        private lateinit var instance: App
        private var appComponent: AppComponent? = null
        private var mainComponent: MainComponent? = null

        fun appComponent(): AppComponent {
            checkNotNull(appComponent) { "AppComponent should be initialized!" }
            return appComponent as AppComponent
        }

        fun initMainComponent(): MainComponent? {
            if (mainComponent != null) return mainComponent

            mainComponent = appComponent!!.mainComponentBuilder()
                .build()
            return mainComponent
        }

        fun mainComponent(): MainComponent {
            checkNotNull(mainComponent) { "Main component should be initialized!" }
            return mainComponent as MainComponent
        }

        fun destroyMainComponent() {
            mainComponent = null
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        initAppComponent()
    }

    private fun initAppComponent() {
        appComponent = DaggerAppComponent.builder()
            .contextModule(
                ContextModule(
                    instance
                )
            )
            .build()
    }
}