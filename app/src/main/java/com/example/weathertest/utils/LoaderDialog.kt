package com.example.weathertest.utils

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import com.example.weathertest.R

class LoaderDialog : DialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_FRAME, R.style.DialogStyle)
        isCancelable = false
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val result = super.onCreateDialog(savedInstanceState)
        result.requestWindowFeature(Window.FEATURE_NO_TITLE)
        result.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        result.window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        return result
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_progress, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {}


    companion object {
        fun newInstance(): LoaderDialog {
            val args = Bundle()
            val fragment = LoaderDialog()
            fragment.arguments = args
            return fragment
        }

        fun setVisibility(activity: FragmentActivity, visible: Boolean) {
            if (visible) {
                show(activity)
            } else {
                hide(activity)
            }
        }

        private fun show(activity: FragmentActivity) {
            val manager = activity.supportFragmentManager
            val fragment = manager.findFragmentByTag(LoaderDialog::class.java.simpleName)
            if (fragment != null) {
                manager.beginTransaction().remove(fragment)
                    .add(newInstance(), LoaderDialog::class.java.simpleName)
                    .commitAllowingStateLoss()
                return
            }
            val ft = manager.beginTransaction()
            ft.add(newInstance(), LoaderDialog::class.java.simpleName)
            ft.commitAllowingStateLoss()

        }

        private fun hide(activity: FragmentActivity) {
            val manager = activity.supportFragmentManager
            val fragment = manager.findFragmentByTag(LoaderDialog::class.java.simpleName)
            if (fragment != null) {
                manager.beginTransaction().remove(fragment).commitAllowingStateLoss()
            }
        }
    }
}
