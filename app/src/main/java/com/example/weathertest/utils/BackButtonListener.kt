package com.example.weathertest.utils

interface BackButtonListener {
    fun onBackPressed(): Boolean
}
