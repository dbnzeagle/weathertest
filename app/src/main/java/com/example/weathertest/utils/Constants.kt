package com.example.weathertest.utils

import com.example.weathertest.data.network.objects.City
import java.util.*

object Constants {

    val defaultCities = listOf(
        City(
            name = "Moscow",
            id = 524901
        ), City(
            name = "Minsk",
            id = 625144
        )
    )

    fun getAssetPath(q: String) = "file:///android_asset/${q.toLowerCase(Locale.getDefault())}.jpeg"
    fun getWeatherIcon(q: String) = "https://openweathermap.org/img/wn/$q@2x.png"

}