package com.example.weathertest.utils

enum class ImageEnum {
    CLEAR_SKY,
    CLOUDS,
    FEW_CLOUDS,
    MIST,
    RAIN,
    SNOW,
    THUNDER
}