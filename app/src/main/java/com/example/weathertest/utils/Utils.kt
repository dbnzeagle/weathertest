package com.example.weathertest.utils


object Utils {

    fun assetName(weatherState: ImageEnum) = when (weatherState) {
        ImageEnum.CLEAR_SKY -> "clear_sky"
        ImageEnum.CLOUDS -> "clouds"
        ImageEnum.FEW_CLOUDS -> "clouds"
        ImageEnum.MIST -> "mist"
        ImageEnum.RAIN -> "rain"
        ImageEnum.SNOW -> "snow"
        ImageEnum.THUNDER -> "thunder"
    }
}