package com.example.weathertest.di.modules

import com.example.weathertest.data.repository.Repository
import com.example.weathertest.data.repository.city.CityRepository
import com.example.weathertest.data.repository.city.CityRepositoryImpl
import com.example.weathertest.data.storage.AppDatabase
import com.example.weathertest.data.storage.PreferencesManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideCityRepository(
        appDatabase: AppDatabase,
        source: Repository,
        prefs:PreferencesManager
    ): CityRepository {
        return CityRepositoryImpl(appDatabase.cityDao(), source,prefs)
    }


}