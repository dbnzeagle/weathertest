package com.example.weathertest.di

import com.example.weathertest.ui.MainActivity
import com.example.weathertest.ui.cities.CitiesFragment
import com.example.weathertest.ui.cities.details.CityDetailsFragment
import dagger.Subcomponent

@MainScope
@Subcomponent(
    modules = []
)
interface MainComponent {

    fun inject(mainActivity: MainActivity)
    fun inject(citiesFragment: CitiesFragment)
    fun inject(citiesDetailsFragment: CityDetailsFragment)

    @Subcomponent.Builder
    interface Builder {
        fun build(): MainComponent
    }
}