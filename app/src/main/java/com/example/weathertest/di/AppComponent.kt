package com.example.weathertest.di

import com.example.weathertest.di.modules.ContextModule
import com.example.weathertest.di.modules.DatabaseModule
import com.example.weathertest.di.modules.NetworkModule
import com.example.weathertest.di.modules.RepositoryModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [ContextModule::class, DatabaseModule::class, NetworkModule::class, RepositoryModule::class ]
)
interface AppComponent {
    fun mainComponentBuilder(): MainComponent.Builder
}