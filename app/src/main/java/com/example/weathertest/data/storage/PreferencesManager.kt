package com.example.weathertest.data.storage

import android.content.Context
import android.content.SharedPreferences
import androidx.annotation.Nullable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PreferencesManager @Inject constructor(private val context: Context) {
    companion object {
        private const val PREF_NAME = "com.example.weathertest.PREFS"
    }

    private val prefs: SharedPreferences

    init {
        this.prefs = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    }


    fun saveLong(key: String, value: Long) {
        val editor = prefs.edit()
        editor.putLong(key, value)
        editor.apply()
    }

    @Nullable
    fun getLong(key: String): Long {
        return prefs.getLong(key, -1)
    }

    fun saveBool(key: String, value: Boolean) {
        val editor = prefs.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    fun getBoolean(key: String): Boolean {
        return prefs.getBoolean(key, false)
    }

    fun removeAll() {
        prefs.edit().apply {
            prefs.all.keys.forEach loop@{ key: String? ->
                remove(key)
            }
        }.apply()
    }

}