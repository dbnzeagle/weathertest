package com.example.weathertest.data.network.objects

import com.google.gson.annotations.SerializedName

data class Sys(
    @SerializedName("timezone")
    val timezone: Long
)