package com.example.weathertest.data.repository.city

import com.example.weathertest.data.network.objects.City
import com.example.weathertest.data.repository.Repository
import com.example.weathertest.data.storage.PreferencesManager
import com.example.weathertest.data.storage.dao.CityDao
import com.example.weathertest.utils.Constants
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CityRepositoryImpl @Inject constructor(
    private val cityDao: CityDao,
    private val repository: Repository,
    private val prefs: PreferencesManager
) : CityRepository {

    override fun getCities(): Single<List<City>> {
        return cityDao.getAll().flatMap {
            val isFirst = prefs.getBoolean("IS_FIRST")
            if (it.isEmpty() && isFirst) {
                repository.getCities(Constants.defaultCities)
                    .flatMap { cities ->
                        prefs.saveBool("IS_FIRST", false)
                        cityDao.updateAll(cities.list).toSingle { cities.list }
                    }
            } else {
                Single.just(it)
            }

        }
    }

    override fun getCity(id: Long): Single<City> {
        return cityDao.getItem(id)
    }

    override fun addCity(city: City): Completable {
        return cityDao.update(city)
    }

    override fun addCities(cities: List<City>): Completable {
        return cityDao.updateAll(cities)
    }

    override fun deleteCity(city: City): Completable {
        return cityDao.delete(city)
    }
}