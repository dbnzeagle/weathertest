package com.example.weathertest.data.network.objects

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class CurrentWeather(
    @SerializedName("id")
    val id: Long,

    @SerializedName("coord")
    val coordinates: Coordinates,

    @SerializedName("weather")
    val weather: List<Weather>,

    @SerializedName("base")
    val base: String,

    @SerializedName("main")
    val weatherMain: WeatherMain,

    @SerializedName("visibility")
    val visibility: Long,

    @SerializedName("wind")
    val wind: Wind,

    @SerializedName("clouds")
    val clouds: Clouds,

    @SerializedName("dt")
    val dt: Long,

    @SerializedName("timezone")
    val timezone: Long,

    @SerializedName("name")
    val name: String,

    @SerializedName("cod")
    val cod: Long
)