package com.example.weathertest.data.network.objects

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "city")
data class City(
    @PrimaryKey
    @SerializedName("id")
    val id: Long,

    @SerializedName("name")
    val name: String,

    @Embedded
    @SerializedName("coord")
    val coordinates: Coordinates? = null,

    @Embedded
    @SerializedName("main")
    val weatherMain: WeatherMain? = null,

    @Embedded
    @SerializedName("wind")
    val wind: Wind? = null,

    @SerializedName("weather")
    val weather: List<Weather>? = null,

    @SerializedName("dt")
    val dt: Long? = null,

    @Embedded
    @SerializedName("sys")
    val sys: Sys? = null
)