package com.example.weathertest.data.storage

import androidx.room.TypeConverter
import com.example.weathertest.data.network.objects.CurrentWeather
import com.example.weathertest.data.network.objects.Weather
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class Converters {

    @TypeConverter
    fun fromWeatherList(data: List<Weather>?): String {
        return Gson().toJson(data)
    }

    @TypeConverter
    fun toWeatherList(str: String?): List<Weather> {
        val type = object : TypeToken<List<Weather>>() {}.type
        return Gson().fromJson(str, type)
    }

    @TypeConverter
    fun fromCurrentWeatherList(data: List<CurrentWeather>?): String {
        return Gson().toJson(data)
    }

    @TypeConverter
    fun toCurrentWeatherList(str: String?): List<CurrentWeather> {
        val type = object : TypeToken<List<CurrentWeather>>() {}.type
        return Gson().fromJson(str, type)
    }
}