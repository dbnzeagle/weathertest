package com.example.weathertest.data.storage.dao

import androidx.room.*
import com.example.weathertest.data.network.objects.City
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface CityDao {

    @Query("SELECT * FROM city")
    fun getAll(): Single<List<City>>

    @Query("SELECT * FROM city WHERE id=:id")
    fun getItem(id: Long): Single<City>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun update(city: City): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateAll(cities: List<City>): Completable

    @Delete
    fun delete(city: City): Completable
}