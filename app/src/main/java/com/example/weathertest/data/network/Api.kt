package com.example.weathertest.data.network

import com.example.weathertest.data.network.objects.City
import com.example.weathertest.data.network.objects.CityResponse
import com.example.weathertest.data.network.objects.CurrentWeather
import com.example.weathertest.data.network.objects.Forecast
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET("weather")
    fun getCityByName(
        @Query("q") q: String
    ): Single<CurrentWeather>

    @GET("find")
    fun searchCity(
        @Query("q") q: String
    ): Single<CityResponse>

    @GET("weather")
    fun getCity(
        @Query("id") id: Long
    ): Single<City>

    @GET("group")
    fun getCities(
        @Query("id") id: String
    ): Single<CityResponse>

    @GET("onecall")
    fun getForecast(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("exclude") exclude: String
    ): Single<Forecast>
}