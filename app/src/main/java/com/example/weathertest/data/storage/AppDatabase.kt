package com.example.weathertest.data.storage

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.weathertest.data.storage.dao.CityDao
import com.example.weathertest.data.network.objects.City
import com.example.weathertest.data.network.objects.CurrentWeather
import com.example.weathertest.data.network.objects.Forecast


@Database(
    entities = [City::class],
    version = 2
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    companion object {
        const val DB_NAME = "weather_database"
    }

    abstract fun cityDao(): CityDao

}