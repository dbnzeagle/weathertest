package com.example.weathertest.data.network

import com.example.weathertest.data.storage.PreferencesManager
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SessionHolder @Inject constructor(
    private val preferencesManager: PreferencesManager
) {
    companion object {
        private const val SESSION_KEY = "SESSION_KEY"
    }

    fun getLastTimeUpdatedValue(): Long {
        return preferencesManager.getLong(
            SESSION_KEY
        )
    }

    fun saveLastTimeUpdatedValue(lastTimeUpdated: Long) {
        preferencesManager.saveLong(SESSION_KEY, lastTimeUpdated)
    }

}
