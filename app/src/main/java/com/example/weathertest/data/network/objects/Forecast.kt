package com.example.weathertest.data.network.objects


import com.google.gson.annotations.SerializedName

data class Forecast(
    @SerializedName("id")
    val id: Long,

    @SerializedName("timezone")
    val timezone: String,

    @SerializedName("current")
    val current: ForecastCurrent,

    @SerializedName("daily")
    val daily: List<ForecastDetail>,

    @SerializedName("cnt")
    val cnt: Int

)