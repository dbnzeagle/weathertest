package com.example.weathertest.data.network.objects

import com.google.gson.annotations.SerializedName

data class CityResponse(
    @SerializedName("cnt")
    val cnt: Long,

    @SerializedName("list")
    val list: List<City>
)