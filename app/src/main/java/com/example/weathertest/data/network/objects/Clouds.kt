package com.example.weathertest.data.network.objects

import com.google.gson.annotations.SerializedName

data class Clouds (
    @SerializedName("all")
    val all: Long
)