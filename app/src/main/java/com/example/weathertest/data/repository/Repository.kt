package com.example.weathertest.data.repository

import com.example.weathertest.data.network.Api
import com.example.weathertest.data.network.SessionHolder
import com.example.weathertest.data.network.objects.*
import io.reactivex.Single
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Repository @Inject constructor(
    private val api: Api, private val sessionHolder: SessionHolder
) {


    fun getCityByName(name: String): Single<CurrentWeather> {
        return api.getCityByName(name)
    }

    fun getCities(cities: List<City>): Single<CityResponse> {
        val ids = cities.map { return@map it.id }
        return api.getCities(ids.joinToString(separator = ","))
    }

    fun searchCity(q: String): Single<CityResponse> {
        return api.searchCity(q)
    }

    fun getCity(id: Long): Single<City> {
        return api.getCity(id)
    }


    fun getForecast(coordinates: Coordinates): Single<Forecast> {
        return api.getForecast(coordinates.lat, coordinates.lon, "minutely,hourly")
    }
}
