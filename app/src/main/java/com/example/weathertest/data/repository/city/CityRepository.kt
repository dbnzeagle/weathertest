package com.example.weathertest.data.repository.city

import com.example.weathertest.data.network.objects.City
import com.example.weathertest.data.repository.Repository
import com.example.weathertest.data.storage.AppDatabase
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

interface CityRepository {
    fun getCities(): Single<List<City>>

    fun getCity(id: Long): Single<City>

    fun addCity(city: City): Completable

    fun addCities(cities: List<City>): Completable

    fun deleteCity(city: City): Completable
}